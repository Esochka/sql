
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Tests {

    @Test
    public void getCountPersonTest()  {
        Main.mySQLClient();
        Assert.assertEquals(5, Main.getCountPerson());
    }

    @Test
    public void getResAgePersonTest()  {
        Main.mySQLClient();
        Assert.assertEquals(19, Main.getResAgePerson());
    }


    @Test
    public void getSortByLnamePersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("banyas");
        arrayList.add("gbg");
        arrayList.add("kruvets");
        arrayList.add("Lutsenko");
        arrayList.add("tutechko");
        Assert.assertEquals(arrayList, Main.getSortByLnamePerson());
    }


    @Test
    public void getCountLnamePersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("banyas");
        arrayList.add("gbg");
        arrayList.add("kruvets");
        arrayList.add("Lutsenko");
        arrayList.add("tutechko");
        Assert.assertEquals(arrayList, Main.getSortByLnamePerson());
    }

    @Test
    public void geLnameBLnamePersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("gbg");
        arrayList.add("banyas");
        Assert.assertEquals(arrayList, Main.geLnameBLnamePerson());
    }

    @Test
    public void geBomgPersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("31");
        arrayList.add("maks");
        arrayList.add("tutechko");
        arrayList.add("19");
        Assert.assertEquals(arrayList.toString(), Main.geBomgPerson().toString());
    }

    @Test
    public void geNoAgePersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("51");
        arrayList.add("Igor");
        arrayList.add("gbg");
        arrayList.add("10");
        arrayList.add("11");
        Assert.assertEquals(arrayList.toString(), Main.geNoAgePerson().toString());
    }

    @Test
    public void getStreetListPersonTest()  {
        Main.mySQLClient();
        Map<String, String> map = new HashMap<>();
        map.put("Polonne","1");
        map.put("123456","1");
        map.put("procpect pravdu","1");
        map.put("nayku","1");

        Assert.assertEquals(map.toString(), Main.getStreetListPerson().toString());
    }

    @Test
    public void getStreetthreePersonTest()  {
        Main.mySQLClient();
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("123456");
        arrayList.add("nayku");
        arrayList.add("Polonne");
        arrayList.add("procpect pravdu");
        Assert.assertEquals(arrayList.toString(), Main.getStreetthreePerson().toString());
    }


}
