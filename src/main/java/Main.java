import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws SQLException {
        mySQLClient();
        System.out.println(getCountPerson());
        System.out.println(getResAgePerson());
        System.out.println(getSortByLnamePerson());
        System.out.println(getCountLnamePerson());
        System.out.println(geLnameBLnamePerson());
        System.out.println(geBomgPerson());
        System.out.println(geNoAgePerson());
        System.out.println(getStreetListPerson());
        System.out.println(getStreeSixPerson());
        System.out.println(getStreetthreePerson());
    }

    static final String URL = "jdbc:mysql://eu-cdbr-west-03.cleardb.net:3306/heroku_174735cab0dd328";
    static final String USER = "b9cd6b8ca36f20";
    static final String PASSWORD = "44cfb7d6";


    private static Connection con;
    private static Statement stmt;
    private static PreparedStatement preparedStatement;
    private static ResultSet rs;

    public static void mySQLClient() {
        try {
            con = DriverManager.getConnection(URL, USER, PASSWORD);
            stmt = con.createStatement();
        } catch (SQLException sqlEx) {
            System.out.println("Connection Failed! Check output console");
            sqlEx.printStackTrace();
        }

        if (con != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }


    }

    public static int getCountPerson() {
        try {

            String query = "select count(*) from person";
            int count = 0;
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                count = rs.getInt(1);
            }
            return count;

        } catch (SQLException e) {
            e.printStackTrace();
            return 0;

        }
    }

    public static int getResAgePerson()  {
        try {
        String query = "select AVG(age) from person";
        int count = 0;
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);
        while (rs.next()) {
            count = rs.getInt(1);
        }
        return count;

    } catch (SQLException e) {
        e.printStackTrace();
        return 0;

    }
    }

    public static ArrayList<String> getSortByLnamePerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT DISTINCT lastName FROM person ORDER BY lastName ASC";
        int count = 0;
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);

        while (rs.next()) {
            arrayList.add(rs.getString(1));

        }

    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }

    public static Map<String, String> getCountLnamePerson()  {
        Map<String, String> map = new HashMap<>();
        try {
        String query = "SELECT lastName, COUNT(*) FROM person GROUP BY lastName";
        int count = 0;
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);
        ArrayList<String> arrayList = new ArrayList<>();

        while (rs.next()) {

            map.put(rs.getString(1), rs.getString(2));

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return map;
    }

    public static ArrayList<String> geLnameBLnamePerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT lastName FROM person WHERE lastName LIKE '%b%'";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);

        while (rs.next()) {
            arrayList.add(rs.getString(1));
        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }

    public static ArrayList<String> geBomgPerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT * FROM person WHERE idStreet IS NULL";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);


        while (rs.next()) {
            String persons = "";
            persons = rs.getString(1) + ", " + rs.getString(2) + ", " + rs.getString(3) + ", " + rs.getString(4);
            arrayList.add(persons);

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }


    public static ArrayList<String> geNoAgePerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT * FROM person JOIN street ON person.idStreet = street.id\n" +
                "WHERE UPPER(street.name) LIKE 'procpect pravdu' AND person.age < 18";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);


        while (rs.next()) {
            String persons = "";
            persons = rs.getString(1) + ", " + rs.getString(2) + ", " + rs.getString(3) + ", " + rs.getString(4) + ", " + rs.getString(5);
            arrayList.add(persons);

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }


    public static Map<String, String> getStreetListPerson()  {
        Map<String, String> map = new HashMap<>();
        try {
        String query = "SELECT street.name, COUNT(street.id) FROM street , person WHERE street.id = person.idStreet " +
                "GROUP BY street.name";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);

        while (rs.next()) {

            map.put(rs.getString(1), rs.getString(2));

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return map;
    }


    public static ArrayList getStreeSixPerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT * FROM street WHERE LENGTH(name) = 6";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);


        while (rs.next()) {

            arrayList.add(rs.getString(1));

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }

    public static ArrayList<String> getStreetthreePerson()  {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
        String query = "SELECT street.name FROM street JOIN person ON street.id = person.idStreet" +
                " GROUP BY street.name" +
                " HAVING COUNT(person.id) < 3";
        stmt = con.createStatement();
        rs = stmt.executeQuery(query);


        while (rs.next()) {

            arrayList.add(rs.getString(1));

        }
    } catch (SQLException e) {
        e.printStackTrace();

    }
        return arrayList;
    }




}
